﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="convert temp" Type="Folder" URL="../convert temp">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="dice_main.vi" Type="VI" URL="../Dice/dice_main.vi"/>
		<Item Name="P1&amp;P2 Mean.vi" Type="VI" URL="../Dice/subvi/P1&amp;P2 Mean.vi"/>
		<Item Name="win rate.vi" Type="VI" URL="../Dice/subvi/win rate.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="DICE.vi" Type="VI" URL="../../../Desktop/新增資料夾 (2)/DICE.vi"/>
			<Item Name="WIN.vi" Type="VI" URL="../../../Desktop/新增資料夾 (2)/WIN.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
