These files are part of the labview control software for the 
class (Design and application of virtual instruments) in 2019 spring of National Taiwan university .

## Install
The code is used on LabVIEW 2015.

---

## Question of convert temp
1. Build a subvi named **thermometer.vi**, which randomly output a Celsius temperature value in between the lower and upper bound set with input parameters of lower bound value,and upper bound value (2 inputs and 1 output).
2. Build a subvi named to **Fahrenheit.vi**, which converts a Celsius temperature reading to a Fahrenheit temperature reading. (one input and one output).
3. Build a **top level.vi**, shows the temperature values got from **thermometer.vi** with a waveform chart in continuous run mode. 
4. On the panel, user can select the display of temperature scale in Celsius or Fahrenheit and summer or winter mode with different upper/lower bounds hard coded inside the program (set appropriate values on your own) 
5. Show second waveform chart for the current temprature, the running average temperatures in past 5 loops, the average values, the maximum and minimum values of all past temperature values on the front panel and on the waveform chart with multiple-plot. 
   Please do not use the built-in mean and max-min function node and implement the minimum, maximum function by yourself (with shift register and comparison in the loop).

## Question of dice
1. Write a dice rolling game with two players which stops when user pushes the stop button. Time delay can be used to slow the game down.This game should include: 
2. A **sub VI** which randomly rolls a dice for each player, when player1 or player2 push the roll butoons on top VI.
3. A second **sub VI** which compares the result to mark the winner, or if it is a tie game.
4. On the **top VI**, record the average dice number rolled for each player. Calculate the win rate and tie rate. Also record the past 5 dice numbers for each player.
5. Show the win rate of player 1 and player 2, and the tie rate with a multiple plot waveform chart.  
---

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).